module ssh

go 1.18

require (
	github.com/melbahja/goph v1.3.0
	github.com/pkg/sftp v1.13.5
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
)

require (
	github.com/kr/fs v0.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
)
