# ssh client

# Build

* Linux
```bash
GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" -trimpath
```

* Windows
```bash
GOOS=windows GOARCH=amd64 go build -ldflags "-s -w" -trimpath
```

* MacOS
```bash
GOOS=darwin GOARCH=amd64 go build -ldflags "-s -w" -trimpath
```


# Basic Usage

```bash
./ssh -ip x.x.x.x -port 443 -user vay3t -pass
```
